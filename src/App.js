import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';

import './App.css';
import Landing from './views/index';
import Login from './views/Login';
import SignUp from './views/SignUp';
import Dashboard from './views/Dashboard';

function App() {
  return (
    <div className="page-back">
      <Dashboard />
  {/*<Landing />*/}
      {/*<SignUp />*/}
    </div>
  );
}

export default App;
