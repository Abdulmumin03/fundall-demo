import React, { Component } from 'react';
import { Container, Row, Col, Form, Label, Input, FormGroup, Button } from 'reactstrap';

export default class SignUp extends Component {
  render() {
    return (
      <div>

      <Container style={{marginTop:"30px"}}>
      <Row>
      <img src={require("../assets/logo.png")} />
      </Row>
      </Container>
        <Container style={{marginTop:"50px"}}>
            
            <Row>
                <Col lg="5" sm="12" md="5">
                <div className="d-flex justify-content-center">
                <img src={require("../assets/signup.png")} />
            </div>
            <div className="text-left" style={{marginTop:"50px", marginBottom:"100px"}}>
                <h1><span className="welcome">Welcome! </span> Let's get to know you.</h1>
                <h5>Your first step toward a better financial lifestyle starts here</h5>
            </div>
                </Col>
                <Col lg="7" sm="12" md="7">
                    <div className="form-back">
                        <Form>
                        <Row>
                            <Col lg="6" md="6" sm="6">
                                <FormGroup>
                                    <Label for="firstname">First Name</Label>
                                    <Input type="text" className="newInputBorder" id="firstname" name="firstname" placeholder="Enter First Name" />
                                </FormGroup>
                            </Col>
                            <Col lg="6" md="6" sm="6">
                            <FormGroup>
                            <Label for="lastname">Last Name</Label>
                            <Input type="text" id="lastname" className="newInputBorder" name="lastname" placeholder="Enter Last Name" />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg="12" md="12" sm="12">
                            <FormGroup>
                            <Label for="email">First Name</Label>
                            <Input type="email" id="email" className="newInputBorder" name="email" placeholder="Enter Email" />
                        </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col lg="12" md="12" sm="12">
                            <FormGroup>
                            <Label for="password">Password</Label>
                            <Input type="password" id="password" className="newInputBorder" name="password" placeholder="Enter Password" />
                        </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col lg="12" md="12" sm="12">
                            <FormGroup>
                            <Label for="cpassword">Confirm Password</Label>
                            <Input type="passsword" className="newInputBorder" id="cpassword" name="cpassword" placeholder="Confirm Password" />
                        </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col lg="12" md="12" sm="12">
                                <Button type="submit" className="text-uppercase form-control newInputBorder site-buttons">Sign Up</Button>
                            </Col>
                        </Row>
                        </Form>
                            <p className="text-center" style={{marginTop:"30px"}}>Already have an account? <a href="#">Login Here</a></p>
                        
                    </div>
                    <div className="text-center" style={{marginTop:"20px"}}>
                        <p>By clicking on Sign Up, you agree to our <a href="#">Terms & Conditions</a> and <a href="#"> Privacy Policy</a></p>
                    </div>
                </Col>
            </Row>
        </Container>
      </div>
    )
  }
}
