import React, { Component } from 'react';
import { Container, Row, Col, Form, Label, Input, FormGroup, Button } from 'reactstrap';

export default class Login extends Component {
  render() {
    return (
      <div>
      <Container style={{marginTop:"30px"}}>
      <Row>
      <img src={require("../assets/logo.png")} />
      </Row>
      </Container>
      <Container style={{marginTop:"50px"}}>
      
      <Row>
          <Col lg="5" sm="12" md="5">
          <div className="" style={{marginTop:"30px"}}>
          <img src={require("../assets/login.png")} />
      </div>
      <div className="text-left" style={{marginTop:"50px", marginBottom:"100px"}}>
          <h1><span className="welcome">Welcome back! </span><br/> We miss you.</h1>
      </div>
          </Col>
          <Col lg="7" sm="12" md="7">
              <div className="form-back">

              <h2>Holla</h2>
              <h6>Sign in to the vibe!</h6>
                  <Form style={{marginTop:"50px"}}>                  
                  <Row>
                      <Col lg="12" md="12" sm="12">
                      <FormGroup>
                      <Label for="email">First Name</Label>
                      <Input type="email" id="email" className="newInputBorder" name="email" placeholder="Enter Email" />
                  </FormGroup>
                      </Col>
                  </Row>

                  <Row>
                      <Col lg="12" md="12" sm="12">
                      <FormGroup>
                      <Label for="password">Password</Label>
                      <Input type="password" id="password" className="newInputBorder" name="password" placeholder="Enter Password" />
                  </FormGroup>
                      </Col>
                  </Row>

                  <Row>
                      <Col lg="12" md="12" sm="12">
                          <Button type="submit" className="text-uppercase form-control newInputBorder site-buttons">Login</Button>
                      </Col>
                  </Row>
                  </Form>
                      <p className="text-center" style={{marginTop:"30px"}}>Don't have an account? <a href="#">Register Here</a></p>
                      <p className="text-center">By clicking on Login, you agree to our <a href="#">Terms & Conditions</a> and <a href="#"> Privacy Policy</a></p>
              </div>
          </Col>
      </Row>
  </Container>
      </div>
    )
  }
}
