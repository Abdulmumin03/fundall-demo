import React, { Component } from 'react'
import Header from '../components/Header';

import { Container, Row, Col } from 'reactstrap';

export default class Landing extends Component {
  render() {
    return (
      <div>
      <Header />
        <Container>
            <Row>
                <Col md="12" lg="12" sm="12">
                    <div className="d-flex justify-content-center">
                        <img src={require("../assets/signup.png")} />
                    </div>
                    <div className="text-center" style={{marginTop:"50px", marginBottom:"100px"}}>
                        <h1>Fundall Expense Tracket</h1>
                        <h3>Mini Project Frontend</h3>
                        <h5>By Abdulmumin Muslim Idris</h5>
                    </div>
                </Col>
            </Row>
        </Container>
      </div>
    )
  }
}
