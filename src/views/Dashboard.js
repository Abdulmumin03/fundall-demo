import React, { Component } from 'react';
import { Container, Row, Col, Form, Label, Input, FormGroup, Button, Progress, Table } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';

export default class Dashboard extends Component {
  render() {
    return (
      <div>
        <Container style={{marginTop:"20px"}}>
            <Row>
                <img src={require("../assets/logo.png")} />
            </Row>
        </Container>
        <Container style={{marginTop:"30px"}}>
            <Row>
                <Col lg="5" sm="12" md="5">
                <Row>
                <Col lg="3" md="3" sm="3">
                    <div className="d-flex justify-content-center align-items-center" style={{background:"#C4C4C4", borderRadius:"20px", height:"89px", width:"93px"}}>
                        <img src={require("../assets/User.png")} alt="user" style={{width:"28px", height:"32px"}} /><br/>
                    </div>
                </Col>
                <Col lg="9" md="9" sm="9" style={{paddingLeft:"20px"}}>
                    <p>
                        <h4>Abdulmumin Muslim Idris</h4>
                        <small>abdulmumin03@gmail.com </small>
                    </p>
                </Col>
            </Row>

            <Row style={{marginTop:"30px"}}>
                    
                    <Col md="12" lg="12" sm="12">
                    <p>Target Monthly Expenses
                    <br/>
                    <span style={{fontWeight:"bold", fontSize:"20px"}}>N596,000</span>
                    </p>
                        <div ><Progress value={20} color="success" style={{height:"7px", width:"inherit"}} /></div>
                        <div className="" style={{marginTop:"10px", borderRadius:"6px", padding: "10px 10px", background:"#FFFFFF", boxShadow: "0px 0px 6px rgba(77, 232, 151, 0.19)", width:"100%"}}>
                            <div className="">
                                <div>
                                   <p>Daily Expenses Summary</p>
                                   <Table width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td>30 Nov, 2018</td>
                                            <td style={{color:"#4CE895"}}>N30,000</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>30 Nov, 2018</td>
                                            <td style={{color:"#4CE895"}}>N30,000</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>30 Nov, 2018</td>
                                            <td style={{color:"#4CE895"}}>N30,000</td>
                                        </tr>
                                    </tbody>
                                   </Table>
                                </div>
                                
                            </div>
                        </div>
                    </Col>
                
            </Row>
                </Col>
                <Col lg="7" sm="12" md="7">
                    <div className="dashboard-form">
                        <div style={{background:"#FFFFFF", padding:"10px", borderRadius:"6px", boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.082937)"}}>
                        <div className="d-flex justify-content-between">
                        <div style={{}}>
                            <h4><span className="welcome">Welcome back,</span> Abdulmumin</h4>
                            <small>Now, let’s get your expenses for this month</small>
                        </div>
                            <img src={require("../assets/dashboard.png")} style={{marginTop:"-40px"}} />
                    </div>
                        </div>

                        <div style={{padding:"20px"}}>
                            <Form>
                            <Row>
                            <Col lg="8" md="8" sm="12">
                            <FormGroup>
                            <Label for="target">Target Monthly Expenses</Label>
                            <Input type="text" id="target" className="newInputBorder" name="target" placeholder="596,000" />
                        </FormGroup>
                            </Col>
                            </Row>

                            <Row>
                            <Col lg="8" md="8" sm="12">
                            <FormGroup>
                            <Label for="date">Date</Label>
                            <Input type="date" className="newInputBorder" id="date" name="date" placeholder="08/08/2019" />
                        </FormGroup>
                            </Col>
                            </Row>
                                

                                

                                <Row>
                                    <Col lg="12" md="12" sm="12">
                                        <h6>Today's Expenses</h6>
                                        <div className="d-flex justify-content-between">
                                            <Input type="text" id="item" name="item" className="newInputBorder" placeholder="Pizza" style={{marginRight:"15px"}} />
                                            <Input type="number" id="itemAmount"  className="newInputBorder" name="itemAmount" placeholder="10000" />
                                        </div>
                                        <div className="d-flex justify-content-between" style={{marginTop:"15px"}}>
                                            <Input type="text" id="item1" name="item1" className="newInputBorder" placeholder="Textbook" style={{marginRight:"15px"}} />
                                            <Input type="number" id="item1Amount" className="newInputBorder" name="item1Amount" placeholder="10000" />
                                        </div>
                                        <div className="d-flex justify-content-between" style={{marginTop:"15px"}}>
                                            <Input type="text" id="item2" name="item2" className="newInputBorder" placeholder="Tuition Fee" style={{marginRight:"15px"}} />
                                            <Input type="number" id="item2Amount" className="newInputBorder" name="item2Amount" placeholder="20000" />
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <div className="d-flex justify-contents-end" style={{marginTop:"15px"}}>
                                    <FormGroup row>
                                    <Label for="total" className="d-flex justify-content-end" sm={8}>Total Actual Expenses: N</Label>
                                    <Col sm={4}>
                                      <Input type="text" name="total" id="total" placeholder="40,000.00" />
                                    </Col>
                                  </FormGroup>
                                    </div>
                                </Row>

                                <Row>
                                    <Col lg="12" md="12" sm="">
                                    <div className="d-flex justify-content-center" style={{marginTop:"15px"}}>
                                    <Button type="submit" className="text-uppercase form-control newInputBorder site-buttons">Save Today's Expenses</Button>
                                    </div>
                                    </Col>
                                </Row>

                                
                            </Form>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
      </div>
    )
  }
}
