import React, { Component } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    Container,
    NavLink,
  } from "reactstrap";

export default class Header extends Component {

    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }


  render() {
    return (
      <div style={{marginTop:"20px", marginBottom:"50px"}}>
      <Container>
         <Navbar className="header-bg" expand="lg">
          <NavbarBrand href="/">
            <img src={require("../assets/logo.png")} />
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="#" className="btn text-uppercase">Log In</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#" className="btn text-uppercase" style={{background:"#4CE895", borderRadius:"10px"}}>Sign Up</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        </Container>
      </div>
    )
  }
}
